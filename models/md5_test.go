package models

import (
	"testing"
)

func TestGetMD5Hash(t *testing.T) {
	var v string

	v = GetMD5Hash("admin")
	if v != "21232f297a57a5a743894a0e4a801fc3" {
		t.Error("Expected 21232f297a57a5a743894a0e4a801fc3, got ", v)
	}
}
