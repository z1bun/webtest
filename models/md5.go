package models

import (
	"crypto/md5"
	"encoding/hex"
)

//Функция принимает строку и возвращает md5 хэш в виде строки
func GetMD5Hash(text string) string {
	hash := md5.Sum([]byte(text))
	return hex.EncodeToString(hash[:])
}
