package models

import (
	"github.com/astaxie/beego/validation"
)

//Структура для входящего json
type InputJson struct {
	Id   int    `valid:"Required" json:"id"`
	Text string `valid:"Required;MaxSize(100)" json:"text"`
}

//метод для автоматической валидации данных входящей структуры
func (input *InputJson) Valid(v *validation.Validation) {
	if input.Id <= 0 {
		v.SetError("Id", "Должно быть целым положительным числом")
	}
}
