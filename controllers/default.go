package controllers

import (
	"encoding/json"
	"strconv"
	models "webtest/models"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/validation"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) GetMD5() {

	input := models.InputJson{} //создаем экземпляр входящей структуры

	json.Unmarshal(c.Ctx.Input.RequestBody, &input) //парсим входящие данные в созданную структуру

	valid := validation.Validation{}

	if b, _ := valid.Valid(&input); !b { //автоматическая валидация данных
		beego.Debug("Ошибки валидации: ", valid.ErrorsMap) //вывод ошибок валидации
		c.Data["Errors"] = valid.ErrorsMap                 //сохранение ошибок. По задаче явно не требуется, но возможно возвращать в ответ все ошибки валидации
		c.Ctx.Output.SetStatus(500)
		return
	}

	output := models.GetMD5Hash(strconv.Itoa(input.Id) + input.Text + models.GetMD5Hash(strconv.Itoa(input.Id%2))) //генерируем выходную строку, если на предыдущих этапах всё хорошо

	type Response struct {
		Output string `json:"output"`
	} //генерируем структуру для ответа в формате json

	r := Response{Output: output}
	c.Data["json"] = &r
	c.ServeJSON(false) //отдаем json без перевода кодировки (оставляем в utf-8)
}
