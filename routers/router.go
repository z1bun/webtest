package routers

import (
	"webtest/controllers"

	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/md5", &controllers.MainController{}, "post:GetMD5")
}
