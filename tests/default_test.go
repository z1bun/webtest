package test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"runtime"
	"testing"
	_ "webtest/routers"

	"github.com/astaxie/beego"
	. "github.com/smartystreets/goconvey/convey"
)

func init() {
	_, file, _, _ := runtime.Caller(1)
	apppath, _ := filepath.Abs(filepath.Dir(filepath.Join(file, ".."+string(filepath.Separator))))
	beego.TestBeegoInit(apppath)
}

//Интеграционные тесты приложения

// Tестируем запрос POST с правильным телом
func TestMainPOST(t *testing.T) {

	type Expected struct {
		Output string `json:"output"`
	}

	e := Expected{Output: "735ebfa18482e70dd3c784ef174d714b"}

	currentResponse := Expected{}

	rBody := bytes.NewReader([]byte(`{
	  	"id": 25, 
	  	"text": "maintext"
		}`))

	r, _ := http.NewRequest("POST", "/md5", rBody)
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)

	beego.Trace("testing", "TestMainPOST", "Code:", w.Code)

	Convey("Subject: Test Station Endpoint\n", t, func() { //сравниваем код ответа. Должно быть 200
		Convey("Status Code Should Be 200", func() {
			So(w.Code, ShouldEqual, 200)
		})
		Convey("The Result Should Not Be Empty", func() {
			So(w.Body.Len(), ShouldBeGreaterThan, 0) //сравниваем длину тела - должно быть больше 0
		})
		Convey("Expected 735ebfa18482e70dd3c784ef174d714b", func() { //сравниваем то, что получили от сервера с правильным ответом
			json.Unmarshal(w.Body.Bytes(), &currentResponse)
			So(currentResponse, ShouldResemble, e)
		})
	})
}

//Тестируем запросы с GET. Должно выдать ошибки 4хх
func TestMainGET(t *testing.T) {

	rBody := bytes.NewReader([]byte(`{
	  	"id": 25, 
	  	"text": "maintext"
		}`))

	r, _ := http.NewRequest("GET", "/md5", rBody)
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)

	beego.Trace("testing", "TestMainGET", "Code:", w.Code)

	Convey("Subject: Test Station Endpoint\n", t, func() {
		Convey("Status Code Should Be 404", func() { //сравниваем код ответа. Должно быть 404, так как метод для обработки не найден
			So(w.Code, ShouldEqual, 404)
		})
	})
}

//тест на ошибку валидации
func TestMainWrongBody(t *testing.T) {

	rBody := bytes.NewReader([]byte(`{
	  	"id": -1, 
	  	"text": "maintext"
		}`))

	r, _ := http.NewRequest("POST", "/md5", rBody)
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)
	beego.Trace("testing", "TestMainWrongBody", "Code:", w.Code)
	Convey("Subject: Test Station Endpoint\n", t, func() {
		Convey("Status Code Should Be 500", func() {
			So(w.Code, ShouldEqual, 500)
		})
	})
}
